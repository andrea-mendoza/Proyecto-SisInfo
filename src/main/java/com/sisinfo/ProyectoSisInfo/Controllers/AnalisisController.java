package com.sisinfo.ProyectoSisInfo.Controllers;

import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sisinfo.ProyectoSisInfo.Entities.Analisis;
import com.sisinfo.ProyectoSisInfo.Entities.Foto;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.*;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Controller
public class AnalisisController {

    private AnalisisService analisisService;
    private FotoService fotoService;
    private UserService userService;
    private PerfilService perfilService;
    private RecordatorioService recordatorioService;
    private String username;
    private Integer idPDF;

    @Autowired
    public void setAnalisisService(AnalisisService analisisService) {
        this.analisisService = analisisService;
    }

    @Autowired
    public void setFotoService(FotoService fotoService) {
        this.fotoService = fotoService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService) {
        this.recordatorioService = recordatorioService;
    }

    @RequestMapping(value = "/Analisis", method = RequestMethod.GET)
    public String list(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        Iterable<Analisis> listaAnalisis = analisisService.listAllAnalisisByProfile(perfilActual);
        ((ArrayList<Analisis>) listaAnalisis).sort((d1, d2) -> d1.getFecha().compareTo(d2.getFecha()));
        Collections.reverse(((ArrayList<Analisis>) listaAnalisis));
        model.addAttribute("perfiles", perfiles);
        model.addAttribute("listaAnalisis",listaAnalisis);
        model.addAttribute("perfilActual", perfilActual);
        return "mostrarAnalisis";
    }

    @RequestMapping("/nuevoAnalisis")
    String nuevoAnalisis(Model model, @RequestParam("perfilId") String id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfilId", id);
        model.addAttribute("analisis", new Analisis());
        return "formularioAnalisis";
    }

    @RequestMapping(value = "/guardarAnalisis", method = RequestMethod.POST)
    String save(Analisis analisis,@RequestParam("file")MultipartFile[] files, @RequestParam("perfilId") String id,
                RedirectAttributes redirectAttributes) throws IOException {
        analisis.setPerfil(perfilService.getPerfil(Integer.parseInt(id)));
        analisisService.saveAnalisis(analisis);
        byte[] pixel;
        for(int i=0;i<files.length;i++)
        {
            if (!files[i].isEmpty()) {
                pixel = files[i].getBytes();
                Foto foto = new Foto();
                foto.setRegistro(analisis);
                foto.setFoto(pixel);
                fotoService.saveFoto(foto);
            }
        }
        redirectAttributes.addAttribute("perfil", analisis.getPerfil());
        return "redirect:/";
    }
    @RequestMapping("/Analisis/{id}")
    String show(@PathVariable Integer id, Model model) throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        this.idPDF = id;
        model.addAttribute("analisis", analisisService.getAnalisis(id));
        List <Foto> fotos = (List<Foto>)fotoService.listAllFotosById(analisisService.getAnalisis(id));
        if(fotos.isEmpty()){
            model.addAttribute("existenFotos", false);
        }else{
            byte[] encodeBase64;
            String base64Encoded;
            for(int i=0;i<fotos.size();i++)
            {
                encodeBase64 = Base64.encode(fotos.get(i).getFoto());
                base64Encoded = new String(encodeBase64,"UTF-8");
                fotos.get(i).setAuxEncoded(base64Encoded);
            }
            model.addAttribute("existenFotos", true);
            model.addAttribute("firstFoto", fotos.get(0));
            fotos.remove(0);
            model.addAttribute("fotos", fotos);
        }

        return "verAnalisis";
    }

    @RequestMapping("/modificarAnalisis/{id}")
    String modificarAnalisis(@PathVariable Integer id, Model model)throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Analisis analisis = analisisService.getAnalisis(id);
        model.addAttribute("analisis", analisis);
        model.addAttribute("perfilId", analisis.getPerfil().getId().toString());
        List registroFotos = new ArrayList();
        List <Foto> fotos = (List<Foto>)fotoService.listAllFotosById(analisisService.getAnalisis(id));
        if(fotos.isEmpty()){
            model.addAttribute("existenFotos", false);
        }else{
            byte[] encodeBase64;
            String base64Encoded;
            for(int i=0;i<fotos.size();i++)
            {
                encodeBase64 = Base64.encode(fotos.get(i).getFoto());
                base64Encoded = new String(encodeBase64,"UTF-8");
                registroFotos.add(base64Encoded);
            }
            model.addAttribute("existenFotos", true);
            model.addAttribute("firstFoto", registroFotos.get(0));
            registroFotos.remove(0);
            model.addAttribute("fotos", registroFotos);
        }
        return "modificarAnalisis";
    }

    @RequestMapping("/eliminarAnalisis/{id}")
    String eliminar(@PathVariable Integer id) {
        analisisService.deleteAnalisis(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/pdfAnalisis",produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generarPDF() throws DocumentException, IOException {
        Document document = new Document();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PdfPTable table = new PdfPTable(4);

        addTableHeader(table);
        addRows(table);

        PdfWriter.getInstance(document, out);

        document.open();
        document.add(table);
        document.close();

        ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Analisis.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    private void addRows(PdfPTable table) {
        List<Analisis> diagnosticos = (List<Analisis>) analisisService.listAllAnalisis();
        for(int i=0;i<diagnosticos.size();i++){
            table.addCell(diagnosticos.get(i).getTitulo());
            table.addCell(diagnosticos.get(i).getTipoDeAnalisis());
            table.addCell(diagnosticos.get(i).getObservaciones());
            table.addCell(diagnosticos.get(i).getFecha());

        }
    }

    private void addTableHeader(PdfPTable table) {

        Stream.of("Titulo", "Tipo de Analisis","Observaciones","Fecha")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });

    }

    @RequestMapping(value = "/pdfAnalisisDetalles",produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generarPDFDetalle() throws DocumentException, IOException {

        //Fuentes PDF
        Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
                Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
                Font.BOLD);

        Document document = new Document();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("Detalles del analisis",catFont));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Titulo: ", smallBold));
        preface.add(new Paragraph(analisisService.getAnalisis(idPDF).getTitulo()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Tipo de Analisis: ", smallBold));
        preface.add(new Paragraph(analisisService.getAnalisis(idPDF).getTipoDeAnalisis()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Fecha: ", smallBold));
        preface.add(new Paragraph(analisisService.getAnalisis(idPDF).getFecha()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Observaciones: ", smallBold));
        preface.add(new Paragraph(analisisService.getAnalisis(idPDF).getObservaciones()));
        preface.add(new Paragraph(" "));

        PdfWriter.getInstance(document, out);

        document.open();
        document.add(preface);
        document.close();

        ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=DetalleAnalisis.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

}
