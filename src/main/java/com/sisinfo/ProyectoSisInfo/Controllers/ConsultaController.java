package com.sisinfo.ProyectoSisInfo.Controllers;


import com.itextpdf.text.*;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;
import com.sisinfo.ProyectoSisInfo.Entities.Consulta;
import com.sisinfo.ProyectoSisInfo.Entities.Foto;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.InputStreamResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.codec.Base64;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.swing.*;
import javax.swing.filechooser.FileSystemView;
import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Stream;

@Controller
public class ConsultaController {

    private ConsultaService consultaService;
    private FotoService fotoService;
    private UserService userService;
    private PerfilService perfilService;
    private RecordatorioService recordatorioService;
    private String username;

    private Integer idPDF;


    @Autowired
    public void setConsultaService(ConsultaService consultaService) {
        this.consultaService = consultaService;
    }

    @Autowired
    public void setFotoService(FotoService fotoService) {
        this.fotoService = fotoService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }

    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }
    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService){this.recordatorioService = recordatorioService; }

    @RequestMapping(value = "/Consultas", method = RequestMethod.GET)
    public String list(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        Iterable<Consulta> listaConsultas = consultaService.listAllConsultasByProfile(perfilActual);

        ((ArrayList<Consulta>) listaConsultas).sort((d1, d2) -> d1.getFecha().compareTo(d2.getFecha()));
        Collections.reverse(((ArrayList<Consulta>) listaConsultas));
        model.addAttribute("perfiles", perfiles);
        model.addAttribute("listaConsultas",listaConsultas);
        model.addAttribute("perfilActual", perfilActual);
        return "mostrarConsulta";
    }
    @RequestMapping("/nuevaConsulta")
    String nuevaConsulta(Model model, @RequestParam("perfilId") String id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfilId", id);
        model.addAttribute("consulta", new Consulta());
        return "formularioConsulta";
    }

    @RequestMapping(value = "/guardarConsulta", method = RequestMethod.POST)
    String save(Consulta consulta,@RequestParam("file")MultipartFile[] files, @RequestParam("perfilId") String id,
                RedirectAttributes redirectAttributes) throws IOException {
        consulta.setPerfil(perfilService.getPerfil(Integer.parseInt(id)));
        consultaService.saveConsulta(consulta);
        byte[] pixel;
        for(int i=0;i<files.length;i++)
        {
            if (!files[i].isEmpty()) {
                pixel = files[i].getBytes();
                Foto foto = new Foto();
                foto.setRegistro(consulta);
                foto.setFoto(pixel);
                fotoService.saveFoto(foto);
            }
        }
        redirectAttributes.addAttribute("perfil", consulta.getPerfil());
        return "redirect:/";
    }

    @RequestMapping("/Consulta/{id}")
    String show(@PathVariable Integer id, Model model) throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        this.idPDF = id;
        Consulta consulta= consultaService.getConsulta(id);
        model.addAttribute("consulta", consulta);
        List <Foto> fotos = (List<Foto>)fotoService.listAllFotosById(consultaService.getConsulta(id));
        if(fotos.isEmpty()){
            model.addAttribute("existenFotos", false);
        }else{
            byte[] encodeBase64;
            String base64Encoded;
            for(int i=0;i<fotos.size();i++)
            {
                encodeBase64 = Base64.encode(fotos.get(i).getFoto());
                base64Encoded = new String(encodeBase64,"UTF-8");
                fotos.get(i).setAuxEncoded(base64Encoded);
            }
            model.addAttribute("existenFotos", true);
            model.addAttribute("firstFoto", fotos.get(0));
            fotos.remove(0);
            model.addAttribute("fotos", fotos);
        }
        return "verConsulta";
    }

    @RequestMapping("/modificarConsulta/{id}")
    String modificarConsulta(@PathVariable Integer id, Model model)throws UnsupportedEncodingException {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Consulta consulta = consultaService.getConsulta(id);
        model.addAttribute("consulta", consulta);
        model.addAttribute("perfilId", consulta.getPerfil().getId().toString());
        List registroFotos = new ArrayList();
        List <Foto> fotos = (List<Foto>)fotoService.listAllFotosById(consultaService.getConsulta(id));
        if(fotos.isEmpty()){
            model.addAttribute("existenFotos", false);
        }else{
            byte[] encodeBase64;
            String base64Encoded;
            for(int i=0;i<fotos.size();i++)
            {
                encodeBase64 = Base64.encode(fotos.get(i).getFoto());
                base64Encoded = new String(encodeBase64,"UTF-8");
                registroFotos.add(base64Encoded);
            }
            model.addAttribute("existenFotos", true);
            model.addAttribute("firstFoto", registroFotos.get(0));
            registroFotos.remove(0);
            model.addAttribute("fotos", registroFotos);
        }
        return "modificarConsulta";
    }

    @RequestMapping("/eliminarConsulta/{id}")
    String eliminar(@PathVariable Integer id) {
        consultaService.deleteConsulta(id);
        return "redirect:/";
    }

    @RequestMapping(value = "/pdfConsultas",produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generarPDF() throws DocumentException, IOException {
        Document document = new Document();

        ByteArrayOutputStream out = new ByteArrayOutputStream();
        PdfPTable table = new PdfPTable(5);

        addTableHeader(table);
        addRows(table);

        PdfWriter.getInstance(document, out);

        document.open();
        document.add(table);
        document.close();

        ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=Consultas.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }

    private void addRows(PdfPTable table) {
        List<Consulta> diagnosticos = (List<Consulta>) consultaService.listAllConsultas();
        for(int i=0;i<diagnosticos.size();i++){
            table.addCell(diagnosticos.get(i).getTitulo());
            table.addCell(diagnosticos.get(i).getDoctor());
            table.addCell(diagnosticos.get(i).getSintomas());
            table.addCell(diagnosticos.get(i).getObservaciones());
            table.addCell(diagnosticos.get(i).getFecha());

        }
    }

    private void addTableHeader(PdfPTable table) {

        Stream.of("Titulo", "Doctor","Sintomas","Observaciones","Fecha")
                .forEach(columnTitle -> {
                    PdfPCell header = new PdfPCell();
                    header.setBackgroundColor(BaseColor.LIGHT_GRAY);
                    header.setBorderWidth(2);
                    header.setPhrase(new Phrase(columnTitle));
                    table.addCell(header);
                });

    }

    @RequestMapping(value = "/pdfConsultaDetalles",produces = MediaType.APPLICATION_PDF_VALUE)
    ResponseEntity<InputStreamResource> generarPDFDetalle() throws DocumentException, IOException {

        //Fuentes PDF
        Font catFont = new Font(Font.FontFamily.TIMES_ROMAN, 18,
                Font.BOLD);
        Font smallBold = new Font(Font.FontFamily.TIMES_ROMAN, 12,
                Font.BOLD);

        Document document = new Document();

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        Paragraph preface = new Paragraph();
        preface.add(new Paragraph("Detalles de la consulta",catFont));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Titulo: ", smallBold));
        preface.add(new Paragraph(consultaService.getConsulta(idPDF).getTitulo()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Doctor: ", smallBold));
        preface.add(new Paragraph(consultaService.getConsulta(idPDF).getDoctor()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Sintomas: ", smallBold));
        preface.add(new Paragraph(consultaService.getConsulta(idPDF).getSintomas()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Fecha: ", smallBold));
        preface.add(new Paragraph(consultaService.getConsulta(idPDF).getFecha()));
        preface.add(new Paragraph(" "));
        preface.add(new Paragraph("Observaciones: ", smallBold));
        preface.add(new Paragraph(consultaService.getConsulta(idPDF).getObservaciones()));
        preface.add(new Paragraph(" "));

        PdfWriter.getInstance(document, out);

        document.open();
        document.add(preface);
        document.close();

        ByteArrayInputStream bis = new ByteArrayInputStream(out.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Disposition", "inline; filename=DetalleConsulta.pdf");

        return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(new InputStreamResource(bis));
    }
}
