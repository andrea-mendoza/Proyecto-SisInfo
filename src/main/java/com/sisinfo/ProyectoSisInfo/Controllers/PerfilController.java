package com.sisinfo.ProyectoSisInfo.Controllers;

import com.sisinfo.ProyectoSisInfo.Entities.*;
import com.sisinfo.ProyectoSisInfo.Services.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

@Controller
public class PerfilController {

    private PerfilService perfilService;
    private UserService userService;
    private RecordatorioService recordatorioService;
    private String username;

    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService) {
        this.recordatorioService = recordatorioService;
    }

    @RequestMapping(value = "/nuevoPerfil", method = RequestMethod.GET)
    public String list(Model model) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfil", new Perfil());
        return "formularioPerfil";
    }

    @RequestMapping(value = "/guardarPerfil", method = RequestMethod.POST)
    String savePerfil(Perfil perfil, RedirectAttributes redirectAttributes) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        perfil.setUser(userService.findByUsername(this.username));
        perfilService.savePerfil(perfil);
        redirectAttributes.addAttribute("perfil", perfil);
        return "redirect:/";
    }

    @RequestMapping("/modificarPerfil/{id}")
    String modificarPerfil(@PathVariable Integer id, Model model) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfil", perfilService.getPerfil(id));
        return "modificarPerfil";
    }

    @RequestMapping("/eliminarPerfil/{id}")
    String eliminar(@PathVariable Integer id) {
        perfilService.deletePerfil(id);
        return "redirect:/";
    }

    /*@RequestMapping(value = "/verPerfil", method = RequestMethod.GET)
    public String verPerfil(Model model, @RequestParam("perfilV")Perfil perfil) {
        model.addAttribute("perfil", perfilService.getPerfil(perfil.getId()));
        return "verPerfil";
    }*/

    @RequestMapping(value = "/verPerfil/{id}", method = RequestMethod.GET)
    public String verPerfil(Model model, @PathVariable Integer id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfil", perfilService.getPerfil(id));
        return "verPerfil";
    }
}
