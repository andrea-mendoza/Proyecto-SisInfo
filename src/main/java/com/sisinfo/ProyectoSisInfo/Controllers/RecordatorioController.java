package com.sisinfo.ProyectoSisInfo.Controllers;


import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Recordatorio;
import com.sisinfo.ProyectoSisInfo.Services.PerfilService;
import com.sisinfo.ProyectoSisInfo.Services.RecordatorioService;
import com.sisinfo.ProyectoSisInfo.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.IOException;

@Controller
public class RecordatorioController {

    private RecordatorioService recordatorioService;
    private UserService userService;
    private PerfilService perfilService;
    private String username;

    @Autowired
    public void setRecordatorioService(RecordatorioService recordatorioService) {
        this.recordatorioService = recordatorioService;
    }
    @Autowired
    public void setUserService(UserService userService) {
        this.userService = userService;
    }
    @Autowired
    public void setPerfilService(PerfilService perfilService) {
        this.perfilService = perfilService;
    }

    @RequestMapping("/nuevoRecordatorioCitaMedica")
    String nuevoRecordaorioCitaMedica(Model model, @RequestParam("perfilId") String id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfilId", id);
        return "formularioCitaMedica";
    }

    @RequestMapping("/nuevoRecordatorioTomaDeMedicamento")
    String nuevoRecordaorioTomaDeMedicamento(Model model, @RequestParam("perfilId") String id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("perfilId", id);
        return "formularioTomaDeMedicamentos";
    }

    @RequestMapping(value = "/guardarRecordatorio", method = RequestMethod.POST)
    String save(Recordatorio recordatorio, @RequestParam("perfilId") String id,
                RedirectAttributes redirectAttributes) throws IOException {
        recordatorio.setPerfil(perfilService.getPerfil(Integer.parseInt(id)));
        recordatorioService.saveRecordatorio(recordatorio);
        redirectAttributes.addAttribute("perfil", recordatorio.getPerfil());
        return "redirect:/Recordatorios";
    }

    @RequestMapping("/Recordatorio/{id}")
    String show(@PathVariable Integer id, Model model) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Recordatorio recordatorio= recordatorioService.getRecordatorio(id);
        model.addAttribute("recordatorio", recordatorio);
        return "verRecordatorio";
    }

    @RequestMapping("/modificarRecordatorio/{id}")
    String modificarRecordatorio(@PathVariable Integer id, Model model) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Recordatorio recordatorio= recordatorioService.getRecordatorio(id);
        model.addAttribute("perfilId", recordatorio.getPerfil().getId().toString());
        model.addAttribute("recordatorio", recordatorio);
        if(recordatorioService.getRecordatorio(id).getTipo().equals ("cita"))
            return "modificarRecordatorioCitas";
        else{
            return  "modificarRecordatorioTomaDeMedicamentos";
        }
    }
    @RequestMapping("/eliminarRecordatorio/{id}")
    String eliminar(@PathVariable Integer id, RedirectAttributes redirectAttributes) {
        redirectAttributes.addAttribute("perfil", recordatorioService.getRecordatorio(id).getPerfil());
        recordatorioService.deleteRecordatorio(id);
        return "redirect:/Recordatorios";
    }
    @RequestMapping(value = "/Recordatorios", method = RequestMethod.GET)
    public String todosrecordatorios(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        Iterable<Recordatorio> listRecordatorios = recordatorioService.listAllRecordatoriosByProfile(perfilActual);
        model.addAttribute("listRecordatorios",listRecordatorios);
        model.addAttribute("perfiles", perfiles);
        model.addAttribute("perfilActual", perfilActual);
        return "mostrarTodosRecordatorios";
    }


    @RequestMapping(value = "/verRecordatorio/{id}", method = RequestMethod.GET)
    public String verRecordatorio(Model model,@PathVariable Integer id) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        model.addAttribute("recordatorio", recordatorioService.getRecordatorio(id));
        if(recordatorioService.getRecordatorio(id).getTipo().equals ("cita"))
            return "verRecordatorioCitas";
        else{
            return  "verRecordatorioTomaDeMedicametos";
        }
    }

    @RequestMapping(value = "/Citas", method = RequestMethod.GET)
    public String soloCitas(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        Iterable<Recordatorio> listRecordatorios = recordatorioService.listAllRecordatoriosByProfile(perfilActual);
        model.addAttribute("listRecordatorios",listRecordatorios);
        model.addAttribute("perfiles", perfiles);
        model.addAttribute("perfilActual", perfilActual);
        return "mostrarCitasRecordatorios";
    }

    @RequestMapping(value = "/Medicamentos", method = RequestMethod.GET)
    public String soloMedicamentos(Model model, @RequestParam(value = "perfil", required = false) Perfil perfilActual) {
        this.username = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("listaRecordatoriosCitas", recordatorioService.listAllCitasAlarm(userService.findByUsername(this.username)));
        model.addAttribute("listaRecordatoriosCitasMotivo", recordatorioService.listAllCitasAlarmMotivo(userService.findByUsername(this.username)));
        Iterable<Perfil> perfiles = perfilService.listAllPerfilesByUser(userService.findByUsername(this.username));
        Iterable<Recordatorio> listRecordatorios = recordatorioService.listAllRecordatoriosByProfile(perfilActual);
        model.addAttribute("listRecordatorios",listRecordatorios);
        model.addAttribute("perfiles", perfiles);
        model.addAttribute("perfilActual", perfilActual);
        return "mostrarMedicamentosRecordatorios";
    }
}
