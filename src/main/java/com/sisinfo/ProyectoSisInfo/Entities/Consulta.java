package com.sisinfo.ProyectoSisInfo.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Consulta extends Registro{

    @NotNull
    private String sintomas;

    @NotNull
    private String doctor;

    public Consulta(){
        this.tipo = "Consulta";
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

}
