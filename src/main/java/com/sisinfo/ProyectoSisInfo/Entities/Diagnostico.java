package com.sisinfo.ProyectoSisInfo.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Entity
public class Diagnostico extends Registro{

    @NotNull
    private String doctor;

    @NotNull
    private String padecimiento;

    @NotNull
    private String tratamiento;

    public Diagnostico(){
        this.tipo = "Diagnostico";
    }

    public String getDoctor() {
        return doctor;
    }

    public void setDoctor(String doctor) {
        this.doctor = doctor;
    }

    public String getPadecimiento() {
        return padecimiento;
    }

    public void setPadecimiento(String padecimiento) {
        this.padecimiento = padecimiento;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }
}
