package com.sisinfo.ProyectoSisInfo.Entities;

import javax.persistence.*;

@Entity
public class Foto{
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Integer id;

    @ManyToOne
    @JoinColumn(name = "registro_id")
    private Registro registro;

    @Lob
    @Column(columnDefinition="mediumblob")
    private byte[] foto;

    private String auxEncoded;
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public byte[] getFoto() {
        return foto;
    }

    public void setFoto(byte[] foto) {
        this.foto = foto;
    }

    public Registro getRegistro() {
        return registro;
    }

    public void setRegistro(Registro registro) {
        this.registro = registro;
    }

    public String getAuxEncoded() {
        return auxEncoded;
    }

    public void setAuxEncoded(String auxEncoded) {
        this.auxEncoded = auxEncoded;
    }
}
