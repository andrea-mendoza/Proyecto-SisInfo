package com.sisinfo.ProyectoSisInfo.Entities;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.text.SimpleDateFormat;

@Entity
public class Recordatorio {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    protected Integer id;


    @NotNull
    private String motivo=""; //o titulo

    @NotNull
    private String observaciones="Sin Observaciones";

    @NotNull
    private String tipo=""; //cita medica o toma de medicamentos
    //en caso de ser cita medica el valor se llena como cita de lo contrario se llena como medicamento

    @NotNull
    private boolean estado=false; //si esta completado seria true

    // especifico para la cita medica
    @NotNull
    private String fecha="";
    @NotNull
    private String doctor="";


    //especifico para la toma de medicamentos
    @NotNull
    private String hora = "";
    @NotNull
    private String medicamento="";

    @NotNull
    private String dosis=""; // cantidad de tabletas, de jarabe, etc

    @NotNull
    private Integer repetir=0; //numero entero que representaria cada cuanto hay que tomar el medicamento

    @ManyToOne
    @JoinColumn(name = "perfil_id")
    private Perfil perfil;


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getMotivo() {
        return motivo;
    }

    public void setMotivo(String motivo) {
        this.motivo = motivo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public boolean isEstado() {
        return estado;
    }

    public void setEstado(boolean estado) {
        this.estado = estado;
    }


    public String getMedicamento() {
        return medicamento;
    }

    public void setMedicamento(String medicamento) {
        this.medicamento = medicamento;
    }

    public String getDosis() {
        return dosis;
    }

    public void setDosis(String dosis) {
        this.dosis = dosis;
    }

    public Integer getRepetir() {
        return repetir;
    }

    public void setRepetir(Integer repetir) {
        this.repetir = repetir;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    } public void setHora(String hora) {
        this.hora = hora;
    }

    public String getHora() {return hora;    }


    public String getDoctor() {return doctor; }

    public void setDoctor(String doctor) {this.doctor = doctor;    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
}

