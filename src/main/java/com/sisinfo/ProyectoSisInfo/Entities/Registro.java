package com.sisinfo.ProyectoSisInfo.Entities;


import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.Date;
import java.util.List;

@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class Registro {
    @Id
    @GeneratedValue(strategy = GenerationType.TABLE)
    protected Integer id;

    @NotNull
    protected String titulo;

    @NotNull
    protected String tipo;

    @NotNull
    protected String observaciones;

    @NotNull
    protected String fecha;

    @NotNull
    protected Date fechaSys = new Date();

    @OneToMany(mappedBy = "registro", cascade = CascadeType.ALL)
    private List<Foto> fotos;

    @ManyToOne
    @JoinColumn(name = "perfil_id")
    private Perfil perfil;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getTitulo() {
        return titulo;
    }

    public void setTitulo(String titulo) {
        this.titulo = titulo;
    }

    public String getFecha() {
        return fecha.replace("00:00:00.0", "");
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public Date getFechaSys() {
        return fechaSys;
    }

    public void setFechaSys(Date fechaSys) {
        this.fechaSys = fechaSys;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public String getObservaciones() {
        return observaciones;
    }

    public void setObservaciones(String observaciones) {
        this.observaciones = observaciones;
    }

    public List<Foto> getFotos() {
        return fotos;
    }

    public void setFotos(List<Foto> fotos) {
        this.fotos = fotos;
    }

    public Perfil getPerfil() {
        return perfil;
    }

    public void setPerfil(Perfil perfil) {
        this.perfil = perfil;
    }
}
