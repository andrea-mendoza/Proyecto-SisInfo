package com.sisinfo.ProyectoSisInfo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProyectoSisInfoApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProyectoSisInfoApplication.class, args);
	}
}
