package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Analisis;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface AnalisisRepository extends CrudRepository<Analisis,Integer> {
    @Query("SELECT a FROM Analisis a WHERE a.perfil =:perfilId")
    Iterable<Analisis> findAllByProfile(@Param("perfilId") Perfil perfil);
}
