package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Consulta;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface ConsultaRepository extends CrudRepository<Consulta, Integer> {

    @Query("SELECT c FROM Consulta c WHERE c.perfil =:perfilId")
    Iterable<Consulta> findAllByProfile(@Param("perfilId")Perfil perfilId);
}
