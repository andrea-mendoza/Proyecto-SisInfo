package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Analisis;
import com.sisinfo.ProyectoSisInfo.Entities.Diagnostico;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface DiagnosticoRepository extends CrudRepository<Diagnostico,Integer> {
    @Query("SELECT d FROM Diagnostico d WHERE d.perfil =:perfilId")
    Iterable<Diagnostico> findAllByProfile(@Param("perfilId") Perfil perfil);
}
