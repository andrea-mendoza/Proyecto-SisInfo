package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Foto;
import com.sisinfo.ProyectoSisInfo.Entities.Registro;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;

@Transactional
public interface FotoRepository extends CrudRepository<Foto,Integer> {
    @Query("SELECT f FROM Foto f where f.registro =:registroId")
    Iterable<Foto> findAllById(@Param("registroId")Registro registroId);
}
