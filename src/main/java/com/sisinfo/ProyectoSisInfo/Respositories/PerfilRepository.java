package com.sisinfo.ProyectoSisInfo.Respositories;

import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import sun.misc.Perf;

import javax.transaction.Transactional;

@Transactional
public interface PerfilRepository extends CrudRepository<Perfil, Integer> {

    @Query("SELECT p from Perfil p WHERE p.user = :userId")
    Iterable<Perfil> findAllById(@Param("userId") Users userId);
}