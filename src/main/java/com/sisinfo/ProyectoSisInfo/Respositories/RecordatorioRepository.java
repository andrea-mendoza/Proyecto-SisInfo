package com.sisinfo.ProyectoSisInfo.Respositories;


import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Recordatorio;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import javax.transaction.Transactional;
import java.util.ArrayList;

@Transactional
public interface RecordatorioRepository extends CrudRepository<Recordatorio,Integer> {
    @Query("SELECT r FROM Recordatorio r WHERE r.perfil =:perfilId")
    Iterable<Recordatorio> findAllByProfile(@Param("perfilId") Perfil perfil);

    @Query("SELECT CONCAT(r.hora,' ',r.fecha) FROM Recordatorio r, Perfil p WHERE r.tipo ='cita' AND r.perfil=p.id AND p.user=:userId")
    ArrayList<String> findAllCitasAlarm(@Param("userId") Users user);

    @Query("SELECT CONCAT(p.name,': ',r.motivo) FROM Recordatorio r, Perfil p WHERE r.tipo ='cita' AND r.perfil=p.id AND p.user=:userId")
    ArrayList<String> findAllCitasAlarmMotivo(@Param("userId") Users user);

}
