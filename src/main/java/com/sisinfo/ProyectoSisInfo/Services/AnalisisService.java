package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Analisis;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;

public interface AnalisisService {

    Iterable<Analisis> listAllAnalisis();
    void saveAnalisis(Analisis analisis);

    Analisis getAnalisis(Integer id);

    void deleteAnalisis(Integer id);

    Iterable<Analisis> listAllAnalisisByProfile(Perfil perfil);
}
