package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Analisis;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Respositories.AnalisisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class AnalisisServiceImpl implements AnalisisService {

    private AnalisisRepository analisisRepository;

    @Autowired
    @Qualifier(value = "analisisRepository")
    public void setAnalisisRepository(AnalisisRepository analisisRepository){
        this.analisisRepository=analisisRepository;
    }

    @Override
    public Iterable<Analisis> listAllAnalisis() {
        return analisisRepository.findAll();
    }

    @Override
    public void saveAnalisis(Analisis analisis) {
        analisisRepository.save(analisis);
    }

    @Override
    public Analisis getAnalisis(Integer id) {
        return analisisRepository.findOne(id);
    }

    @Override
    public void deleteAnalisis(Integer id) {
        analisisRepository.delete(id);
    }

    @Override
    public Iterable<Analisis> listAllAnalisisByProfile(Perfil perfil){
        return analisisRepository.findAllByProfile(perfil);
    }
}
