package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Consulta;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;

public interface ConsultaService {

    Iterable<Consulta> listAllConsultas();
    void saveConsulta(Consulta consulta);

    Consulta getConsulta(Integer id);

    void deleteConsulta(Integer id);

    Iterable<Consulta> listAllConsultasByProfile(Perfil perfil);
}
