package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Consulta;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;
import com.sisinfo.ProyectoSisInfo.Respositories.ConsultaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class ConsultaServiceImpl implements ConsultaService {

    private ConsultaRepository consultaRepository;

    @Autowired
    @Qualifier(value = "consultaRepository")
    public void setConsultaRepository(ConsultaRepository consultaRepository){
        this.consultaRepository = consultaRepository;
    }

    @Override
    public Iterable<Consulta> listAllConsultas() {
        return consultaRepository.findAll();
    }

    @Override
    public void saveConsulta(Consulta consulta) {
        consultaRepository.save(consulta);
    }

    @Override
    public Consulta getConsulta(Integer id) {
        return consultaRepository.findOne(id);
    }

    @Override
    public void deleteConsulta(Integer id) {
        consultaRepository.delete(id);
    }

    @Override
    public Iterable<Consulta> listAllConsultasByProfile(Perfil perfil){
        return consultaRepository.findAllByProfile(perfil);
    }
}
