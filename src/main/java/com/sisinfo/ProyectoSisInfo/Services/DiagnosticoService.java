package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Diagnostico;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;

public interface DiagnosticoService {
    Iterable<Diagnostico> listAllDiagnosticos();

    Diagnostico getDiagnosticoById(Integer id);

    Diagnostico saveDiagnostico(Diagnostico diagnostico);

    void deleteDiagnostico(Integer id);

    Iterable<Diagnostico> listAllDiagnosticosByProfile(Perfil perfil);

}
