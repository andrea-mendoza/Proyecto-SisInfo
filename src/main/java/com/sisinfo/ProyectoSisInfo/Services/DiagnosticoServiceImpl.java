package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Diagnostico;
import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Respositories.DiagnosticoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class DiagnosticoServiceImpl implements DiagnosticoService{
    private DiagnosticoRepository diagnosticoRepository;

    @Autowired
    @Qualifier(value = "diagnosticoRepository")
    private  void setDiagnosticoRepository(DiagnosticoRepository diagnosticoRepository){
        this.diagnosticoRepository=diagnosticoRepository;
    }

    @Override
    public Iterable<Diagnostico> listAllDiagnosticos() {
        return diagnosticoRepository.findAll();
    }

    @Override
    public Diagnostico getDiagnosticoById(Integer id) {
        return diagnosticoRepository.findOne(id);
    }

    @Override
    public Diagnostico saveDiagnostico(Diagnostico diagnostico) {
        return diagnosticoRepository.save(diagnostico);
    }

    @Override
    public void deleteDiagnostico(Integer id) {
        diagnosticoRepository.delete(id);
    }

    @Override
    public Iterable<Diagnostico> listAllDiagnosticosByProfile(Perfil perfil){
        return diagnosticoRepository.findAllByProfile(perfil);
    }

}