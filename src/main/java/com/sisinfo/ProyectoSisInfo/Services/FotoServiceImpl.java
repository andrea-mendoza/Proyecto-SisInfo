package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Foto;
import com.sisinfo.ProyectoSisInfo.Entities.Registro;
import com.sisinfo.ProyectoSisInfo.Respositories.FotoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

@Service
public class FotoServiceImpl implements FotoService {

    private FotoRepository fotoRepository;

    @Autowired
    @Qualifier(value = "fotoRepository")
    public void setFotoRepository(FotoRepository fotoRepository){
        this.fotoRepository=fotoRepository;
    }

    @Override
    public Iterable<Foto> listAllFoto() {
        return fotoRepository.findAll();
    }

    @Override
    public void saveFoto(Foto foto) {
        fotoRepository.save(foto);
    }

    @Override
    public Foto getFoto(Integer id) {
        return fotoRepository.findOne(id);
    }

    @Override
    public void deleteFoto(Integer id) {
        fotoRepository.delete(id);
    }
    @Override
    public Iterable<Foto> listAllFotosById(Registro registro){
        return fotoRepository.findAllById(registro);
    }
}
