package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Users;

public interface PerfilService {

    Iterable<Perfil> listAllPerfiles();
    void savePerfil(Perfil perfil);

    Perfil getPerfil(Integer id);

    void deletePerfil(Integer id);

    Iterable<Perfil> listAllPerfilesByUser(Users user);

}
