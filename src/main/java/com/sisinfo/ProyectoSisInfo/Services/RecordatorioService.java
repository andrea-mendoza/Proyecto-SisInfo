package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Perfil;
import com.sisinfo.ProyectoSisInfo.Entities.Recordatorio;
import com.sisinfo.ProyectoSisInfo.Entities.Users;

import java.util.ArrayList;

public interface RecordatorioService {

    Iterable<Recordatorio> listAllRecordatorios();

    Iterable<Recordatorio> listAllRecordatoriosByProfile(Perfil perfil);
    ArrayList<String> listAllCitasAlarm(Users user);
    ArrayList<String> listAllCitasAlarmMotivo(Users user);
    void saveRecordatorio(Recordatorio recordatorio);

    Recordatorio getRecordatorio(Integer id);

    void deleteRecordatorio(Integer id);
}
