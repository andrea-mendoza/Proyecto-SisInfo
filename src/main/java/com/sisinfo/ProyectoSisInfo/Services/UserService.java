package com.sisinfo.ProyectoSisInfo.Services;

import com.sisinfo.ProyectoSisInfo.Entities.Users;

public interface UserService {

    void save(Users user);
    Users findByUsername(String username);

    Iterable<Users> listAllUsers();
}
